import Vue from 'vue'
import VueRouter from 'vue-router'
import DetailPost from "@/components/DetailPost";
import CreatePost from "@/components/CreatePost";
import Register from "@/components/Register";
import ListPost from "@/components/ListPost";
import Login from "@/components/Login";
import Active from "@/components/Active";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/create',
            name: 'create',
            component: CreatePost,
            meta: {requiresAuth: true}
        },
        {
            path: '/post/:id',
            name: 'detail',
            component: DetailPost,
            meta: {requiresAuth: true}
        },
        {
            path: '/',
            name: 'home',
            component: ListPost
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/active/:token',
            name: 'active',
            component: Active
        }]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(path => path.meta.requiresAuth)) {
        if (localStorage.getItem("username")) {
            next()
        } else {
            router.replace('/login')
        }
    } else {
        next()
    }
});

export default router
