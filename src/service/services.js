import axios from "axios";

export const getAllPost = async () => {
    const response = await axios.get(process.env.VUE_APP_API_POST + "/getAll" )
    return response
}

export const login = async (user) => {
    const response = await axios.post( process.env.VUE_APP_API_USER +'/login', user, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
    return response
}

export const createPostRequest = async (post) => {
    const response = await axios.post(process.env.VUE_APP_API_POST + `/create`, post, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " + localStorage.getItem('jwt')
        }
    })
    return response.data.result
}

export const uploadImage = async (formData) =>{
    const response = await axios.post(process.env.VUE_APP_API_IMG + '/save', formData,{
        headers: {
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + localStorage.getItem('jwt')
        }
    })

    return response.data.result
}
export const registerUser = async (user) => {

    const response = await axios.post(process.env.VUE_APP_API_USER + '/signup', user, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })

    return response
}


export const activeAccount = async (token) => {
    const response = await axios.get(process.env.VUE_APP_API_USER + '/active/'+token)
    return response.data.result
}
