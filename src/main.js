import Vue from 'vue'
import App from './App.vue';
import router from "../router";
import { BootstrapVue} from "bootstrap-vue";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import * as VeeValidate from 'vee-validate';
import moment from 'moment'
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment((value)).format('DD/MM/yyyy hh:mm:ss A')
    // return moment((value)).format('ddd DD/MM/YYYY HH:MM:SS')
  }
})

Vue.use(BootstrapVue, VeeValidate)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


